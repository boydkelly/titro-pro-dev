#!/usr/bin/bash

for x in dos2unix curl; do
  type -P $x >/dev/null 2>&1 || { echo >&2 "${x} not installed.  Aborting."; exit 1; }
done

for y in en fr; do
  echo $y
      DATA=data.$y.tsv
      SUFFIX=.$y.md
  case $y in
    "en")
      TAGS="[\"English\"]"
      KEYWORDS="[\"Côte d'Ivoire\", \"Ivory Coast\", \"News\", \"Actualité\", \"Titrologie\", \"Titrologue\"]"
      CATEGORY="[\"News\"]"
      GETFILE="https://docs.google.com/spreadsheets/d/e/2PACX-1vQdBPpAFhf_ehY7FZ47Lm__KdOcZ5ZgngrxDFLA_ETaED3ZenVBmLxcrNS3J6HeotXh9BCB_KzagAuu/pub?gid=0&single=true&output=tsv"
      ;;
    "fr")
      TAGS="[\"français\"]"
      KEYWORDS="[\"Côte d'Ivoire\", \"Ivory Coast\", \"News\", \"Actualité\", \"Titrologie\", \"Titrologue\"]"
      CATEGORY="[\"Actualité\"]"
      GETFILE="https://docs.google.com/spreadsheets/d/e/2PACX-1vQdBPpAFhf_ehY7FZ47Lm__KdOcZ5ZgngrxDFLA_ETaED3ZenVBmLxcrNS3J6HeotXh9BCB_KzagAuu/pub?gid=170035717&single=true&output=tsv"
      ;;
  esac
  curl -s "${GETFILE}" | sed '1d' > ${DATA} || { echo "Download ${DATA} failed"; exit 1; }
  [[ -f "${DATA}" ]] || { echo "${DATA} not found."; exit 1; } 

  count=1
  while IFS=$'\t' read -a line || [[ -n $line ]]; do
    POSTDATE=${line[0]}
    FILE=`date +"%Y-%m-%d"`-`printf "%04x" $RANDOM`-$count${SUFFIX}
    [[ -n $POSTDATE  ]] || POSTDATE=$(date -Is)
    echo "---" > $FILE
    echo "title: \"${line[3]}\"" >> $FILE
    echo "link: \"${line[1]}\"" >> $FILE
    echo "image: \"${line[2]}\"" >> $FILE
    echo "author: ${line[4]}" >> $FILE
    echo "date: $POSTDATE" >> $FILE
    echo "keywords: $KEYWORDS" >> $FILE
    echo "tags: $TAGS" >> $FILE
    echo "categories: $CATEGORY" >> $FILE
    echo "filename: $FILE" >> $FILE
    echo "description: \"${line[3]}\"" >> $FILE
    cat <<EOF >> $FILE
type: post
draft: true 
---
EOF
  let count=count+1
  done < ${DATA}
  rm ${DATA}
done

