#!/usr/bin/bash
[[ -z $1 ]] && { echo "you need an input file" ; } 
DATA=$1
POSTDATE=`date -Im`
FILE=`date +"%Y-%m-%d-%s"`.fr.md

function newfile( ){
cat <<EOF > $FILE
---
title: 
link: 
image: 
author: 
date: 
type: post
description: 
keywords: ["Côte d'Ivoire", "Ivory Coast", "News", "Actualité", "Titrologie", "Titrologue"]
topic: ["Actualité"]
tags: ["français"]
draft: true 
filename:
---
EOF
}

newfile


counter=1
while IFS="" read line || [ -n "$line" ]; 
do
  case $counter in
    1)
      tkey="link:"
      ;;
    2)
      tkey="image:"
      ;;
    3)
      tkey="title:"
      ;;
    4)
      tkey="author:"
      ;;
  esac
  sed -i "s/${tkey}/${tkey} $line/g" $FILE
  echo $counter
  echo $line
  echo $tkey
  let counter+=1
done < $DATA

sed -i "s/date:/date: $POSTDATE/g" $FILE
sed -i "s/filename:/filename: $FiLE/g" $FILE

