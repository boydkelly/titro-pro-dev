---
title: "Terrorism Threat in West Africa Soars as U.S. Weighs Troop Cuts"
link: "https://www.nytimes.com/2020/02/27/world/africa/terrorism-west-africa.html"
image: "https://static01.nyt.com/images/2020/02/25/world/25DC-SAHEL/25DC-SAHEL-mediumThreeByTwo252.jpg?quality=100&auto=webp"
categories: ["nytimes"]
date: 2020-02-27
keywords: ["Côte d'Ivoire", "Ivory Coast", "News", "Actualité", "Titrologie", "Titrologue"]
tags: ["economy"]
filename: 2020-05-23-55dc-1.md
description: "Terrorism Threat in West Africa Soars as U.S. Weighs Troop Cuts"
type: post
draft: false
---
